#include "SQL_CONNETCOR.h"

SQL_CONNETCOR::SQL_CONNETCOR()
{
    sqlConnection = nullptr;
    sqlResult = nullptr;
}

SQL_CONNETCOR::~SQL_CONNETCOR()
{
    if (sqlResult != nullptr)
        mysql_free_result(sqlResult);
    if (sqlConnection != nullptr)
        mysql_close(sqlConnection);
}

unsigned int SQL_CONNETCOR::Init()
{
    if (sqlConnection != nullptr)
        return ERRQ_REINIT;
    if ((sqlConnection = mysql_init(&init))==NULL)
        return ERRQ_NOT_INIT;
    return ERRQ_OK;
}

unsigned int SQL_CONNETCOR::Connect(char* host,
                             char* user,
                             char* password,
                             char* database,
                             unsigned int port,
                             char* unix_socket,
                             unsigned long flags)
{
    if (mysql_real_connect(sqlConnection,
                           host,
                           user,
                           password,
                           database,
                           port,
                           unix_socket,
                           flags) == NULL)
        return ERRQ_CONNECTION;
    return ERRQ_OK;
}

unsigned int SQL_CONNETCOR::Disconnect()
{
    if (sqlConnection == nullptr)
        return ERRQ_DISCONNECT;

    if (sqlResult != nullptr)
        mysql_free_result(sqlResult);
    mysql_close(sqlConnection);
    return ERRQ_OK;
}

unsigned int SQL_CONNETCOR::Query(const char* db_query)
{
    unsigned int queryLen = strnlen(db_query, MAX_QUERY_LEN);
    if (!queryLen)
        return ERRQ_QUERY_DATA;
        //Error: string was formed incorrectly

    if (mysql_query(sqlConnection, db_query))
        return ERRQ_QUERY_SEND;
        //Error: the request was executed incorrectly
    return ERRQ_OK;
}

unsigned int SQL_CONNETCOR::Next()
{
    if (sqlResult == nullptr)
    {
        sqlResult = mysql_store_result(sqlConnection);
        sqlRow = mysql_fetch_row(sqlResult);
    }
    else
        sqlRow = mysql_fetch_row(sqlResult);
    if (sqlRow == NULL)
        return 0;
    else
        return 1;
}

char* SQL_CONNETCOR::Value(unsigned int num_col)
{
    char* res = new char[strlen(sqlRow[num_col])+1];
    strcpy(res, sqlRow[num_col]);
    return res;
}
