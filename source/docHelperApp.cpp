/***************************************************************
 * Name:      docHelperApp.cpp
 * Purpose:   Code for Application Class
 * Author:    SKB ()
 * Created:   2021-05-22
 * Copyright: SKB ()
 * License:
 **************************************************************/

#include "docHelperApp.h"

//(*AppHeaders
#include "docHelperMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(docHelperApp);

bool docHelperApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	docHelperFrame* Frame = new docHelperFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
