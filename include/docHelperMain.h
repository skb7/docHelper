/***************************************************************
 * Name:      docHelperMain.h
 * Purpose:   Defines Application Frame
 * Author:    SKB ()
 * Created:   2021-05-22
 * Copyright: SKB ()
 * License:
 **************************************************************/

#ifndef DOCHELPERMAIN_H
#define DOCHELPERMAIN_H

//(*Headers(docHelperFrame)
#include <wx/calctrl.h>
#include <wx/choice.h>
#include <wx/frame.h>
#include <wx/grid.h>
#include <wx/menu.h>
#include <wx/sizer.h>
#include <wx/statusbr.h>
//*)

#include "authUserDialog.h"
#include "SQL_CONNETCOR.h"

class docHelperFrame: public wxFrame
{
    public:

        docHelperFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~docHelperFrame();

    private:

        //(*Handlers(docHelperFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        //*)

        //(*Identifiers(docHelperFrame)
        static const long ID_CHOICE1;
        static const long ID_CHOICE2;
        static const long ID_GRID1;
        static const long ID_CALENDARCTRL1;
        static const long ID_GRID2;
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(docHelperFrame)
        wxCalendarCtrl* dcal_SelectDay;
        wxChoice* Choice1;
        wxChoice* Choice2;
        wxGrid* Grid2;
        wxGrid* gDataTable;
        wxStatusBar* StatusBar1;
        //*)

        authUserDialog* authDialog;
        SQL_CONNETCOR* dbConnection;

        DECLARE_EVENT_TABLE()
};

#endif // DOCHELPERMAIN_H
